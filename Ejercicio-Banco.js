"use strict";

class Banco {
  constructor(nombre, direccion) {
    this.nombre = nombre;
    this.direccion = direccion;
    this.clientes = [];
  }
}
class CuentaBancaria {
  constructor(id) {
    this.balance = 0;
    this.id = id;
  }
}

class Titular {
  constructor(nombre, genero, monedero) {
    this.nombre = nombre;
    this.genero = genero;
    this.monedero = monedero;
    this.id = Math.floor(Math.random() * 999999999);
  }

  abrirCuentaBancaria(banco) {
    // 1. Generar un ID para la nueva cuenta bancaria.
    const newId = this.id;
    // 2. Acceder al array "clientes" del banco y almacenar el
    //    nombre y el ID de la cuenta bancaria del nuevo cliente
    banco.nombre = "Anyi";
    console.log(newId, banco.nombre);
    // 3. Crear una nueva cuenta bancaria a la que debemos pasar
    //    el nuevo ID.
    banco.clientes.push({ id: newId, nombre: banco.nombre });
  }

  ingresarDinero(cantidad, cuenta) {
    // 1. Comprobar si en el monedero tenemos la cantidad que
    //    deseamos ingresar. De no ser asÃ­, mostrar un mensaje
    //    que diga que no tenemos suficiente dinero en el
    //    monedero.
    if (this.monedero < cantidad) {
      console.log("No tienes suficiente dinero ene el monedrero");
    }
    // 2. Si tenemos suficiente dinero en el monedero, solo
    //    queda restar en el monedero la cantidad que vamos
    //    a ingresar.
    else if (this.monedero >= cantidad) {
      this.monedero -= cantidad;
    }
    // 3. Acceder a la propiedad "balance" de la cuenta bancaria
    //    y sumar la cantidad a ingresar. Mostrar un mensaje de
    //    que el ingreso ha sido realizado.
    cuenta.balance += cantidad;
    console.log("El dinero ha sido ingresado");
  }

  retirarDinero(cantidad, cuenta) {
    // 1. Comprobar si en la propiedad "balance" de nuestra
    //    cuenta tenemos la cantidad que deseamos retirar.
    //    De no ser asÃ­, mostrar un mensaje que diga que
    //    no tenemos suficiente dinero en la cuenta.
    if (cantidad > cuenta.balance) {
      console.log("Fondos insuficientes");
    }
    // 2. Si tenemos suficiente dinero en la cuenta, solo
    //    queda restar en el balance la cantidad que vamos
    //    a retirar.
    else if (cantidad <= cuenta.balance) {
      cuenta.balance -= cantidad;
    }
    // 3. Acceder a la propiedad "monedero" del titular
    //    y sumar la cantidad retirada al monedero. Mostrar
    //    un mensaje de que el ingreso ha sido realizado.
    this.monedero = cuenta.balance;
  }

  mostrarSaldo(cuenta) {
    // 1. Acceder a la propiedad "balance" de la cuenta y
    //    mostrar un mensaje que nos indique nuestro saldo
    //    actual.
    console.log(`Te queda ${cuenta.balance} disponible`);
  }
}

const titular = new Titular();
const miBanco = new Banco();
const miCuentaBancaria = new CuentaBancaria();
titular.abrirCuentaBancaria(miBanco);
titular.ingresarDinero(10000, miCuentaBancaria);
titular.retirarDinero(4200, miCuentaBancaria);
titular.mostrarSaldo(miCuentaBancaria);