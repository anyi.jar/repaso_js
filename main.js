//Ejercicios JS

/* Contains Duplicate
Given an array of integers, find if the array contains any duplicates.
Your function should return true if any value appears at least twice in the array, 
and it should return false if every element is distinct.*/

/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    for(let i=0; i < nums.length; i++){
        for(let j=0; j < i; j++){
            if(nums[i] === nums[j]) return true;
        } 
    }
    return false;
};

//Max Consecutive Ones
/* Given a binary array, find the maximum number of consecutive 1s in this array.*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var findMaxConsecutiveOnes = function(nums) {
    const maxNumb = nums.join('').split(0).map((num) => num.length);
    return Math.max(...maxNumb);
};



//283. Move Zeroes
/* Given an array nums, write a function to move all 0's to the end of it while 
maintaining the relative order of the non-zero elements.
*/

/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function(nums) {   
number = [...nums];
  for(let i = nums.length-1; i>=0; i--){
    if(number[i]===0){
      nums.splice(i, 1);
      nums.push(0);
    }
  };
};
